variable "ec2_id" {
  description = "The id of ec2 instance running"
}

variable "eip_name" {
  description = "The name tag of eip"
}

